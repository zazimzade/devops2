

kubectl taint nodes worker app=blue:NoSchedule

kubectl taint nodes worker app=blue:PreferNoSchedule

kubectl taint nodes worker app=blue:NoExecute

############## if you want remove taint from node ############

kubectl taint nodes worker app=blue:NoSchedule-

kubectl taint nodes worker app=blue:PreferNoSchedule-

kubectl taint nodes worker app=blue:NoExecute-
