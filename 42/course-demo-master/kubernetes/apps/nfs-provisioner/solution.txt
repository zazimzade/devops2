In kubernetes version 1.20 and hier api server dont use self-link.
And for working this provisioner use must edit /etc/kubernetes/manifest/kube-apiserver.yaml.
Only add  "- --feature-gates=RemoveSelfLink=false" this line like option. Kubelet will automatically recreate api-server pod.
